import Swiper from 'swiper';

const newsSlider = new Swiper ('.js-news-slider', {
  init: true,
  loop: true,
  slidesPerView: 1,
  effect: 'fade',
  // Disable preloading of all images
  // preloadImages: false,
  // // Enable lazy loading
  // lazy: true,
  pagination: {
    el: '.js-slider-news-pagination',
    type: 'bullets',
    clickable: true,
  },
  // navigation: {
  //   nextEl: '.js-shops-slider-buy-btn-next-1',
  //   prevEl: '.js-shops-slider-buy-btn-prev-1',
  // },
  // breakpoints: {
  //   220: {
  //     slidesPerView: 2,
  //     spaceBetween: 10
  //   },
  //   800: {
  //     slidesPerView: 3,
  //     spaceBetween: 30
  //   }
  // }
});

export default newsSlider;