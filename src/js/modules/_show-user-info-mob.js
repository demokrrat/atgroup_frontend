$(document).on("click",".js-show-user-info",function(e) {
  e.preventDefault();
  $('.nav-list__item:not(.hide-tablet)').toggleClass('active-user-info');
  $('.nav-list__sticky:not(.first)').toggleClass('active-user-info');
});