import Swiper from 'swiper';

const addproductsSlider = new Swiper ('.js-slider-add-products', {
  init: true,
  
  slidesPerView: 3,
  allowTouchMove: false,
  navigation: {
    nextEl: '.js-slider-add-products-next',
    prevEl: '.js-slider-add-products-prev',
  },
  breakpoints: {
    860: {
      
      allowTouchMove: false,
      slidesPerView: 3,
    },
    640: {
      allowTouchMove: true,
      slidesPerView: 2,
    },
    300: {
      
      allowTouchMove: false,
      slidesPerView: 'auto',
    }
  }
});

export default addproductsSlider;