import Swiper from 'swiper';

const bannerSlider = new Swiper ('.js-slider-banner', {
  init: true,
  loop: true,
  slidesPerView: 1,
  // effect: 'fade',
  // Disable preloading of all images
  preloadImages: false,
  // Enable lazy loading
  lazy: true,
  autoplay: {
    delay: 5000,
    disableOnInteraction: false
  },
  pagination: {
    el: '.js-slider-banner-pagination',
    type: 'bullets',
    clickable: true,
  },
  // navigation: {
  //   nextEl: '.js-shops-slider-buy-btn-next-1',
  //   prevEl: '.js-shops-slider-buy-btn-prev-1',
  // },
  // breakpoints: {
  //   220: {
  //     slidesPerView: 2,
  //     spaceBetween: 10
  //   },
  //   800: {
  //     slidesPerView: 3,
  //     spaceBetween: 30
  //   }
  // }
});

export default bannerSlider;