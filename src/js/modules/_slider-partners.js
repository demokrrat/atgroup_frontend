import Swiper from 'swiper';

const partnerSlider = new Swiper ('.js-partners-item', {
  init: true,
  slidesPerColumn: 4,
  slidesPerView: 1,
  // Disable preloading of all images
  // preloadImages: false,
  // // Enable lazy loading
  // lazy: true,
  pagination: {
    el: '.js-slider-partner-pagination',
    type: 'bullets',
    clickable: true,
  },
  // navigation: {
  //   nextEl: '.js-shops-slider-buy-btn-next-1',
  //   prevEl: '.js-shops-slider-buy-btn-prev-1',
  // },
  // breakpoints: {
  //   220: {
  //     slidesPerView: 2,
  //     spaceBetween: 10
  //   },
  //   800: {
  //     slidesPerView: 3,
  //     spaceBetween: 30 
  //   }
  // }
});

export default partnerSlider;