/* eslint-disable func-names */
import Swiper from 'swiper';

// eslint-disable-next-line func-names
$(document).ready(function () {
  
// eslint-disable-next-line no-unused-vars
$.each( $('.product__img'), function (indexInArray, valueOfElement) { 

  const bigSlider = $(this).find('.js-big-product-slider').addClass(`js-big-slider${indexInArray}`);
  const smallSlider = $(this).find('.js-small-product-slider').addClass(`js-small-slider${indexInArray}`);
  const scrollBar = $(this).find('.swiper-scrollbar').addClass(`js-slider-scrollbar-${indexInArray}`);
  
  const galleryThumbs = new Swiper(smallSlider, {
    spaceBetween: 0,
    slidesPerView: 'auto',
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    scrollbar: {
      el: scrollBar,
      hide: true,
      draggable: true,
      dragSize: 100
    },
  });
  // eslint-disable-next-line no-unused-vars
  const galleryTop = new Swiper(bigSlider, {
    spaceBetween: 0,
    slidesPerView: 1,
    // navigation: {
    //   nextEl: '.swiper-button-next',
    //   prevEl: '.swiper-button-prev',
    // },
    thumbs: {
      swiper: galleryThumbs
    }
  });

});

// eslint-disable-next-line no-unused-vars
$.each( $('.product__imgs'), function (indexInArray, valueOfElement) { 


  
  const bigSlider = $(this).find('.js-product-detail-slider-big').addClass(`js-big-slider${indexInArray}`);
  const smallSlider = $(this).find('.js-product-detail-slider-small').addClass(`js-small-slider${indexInArray}`);
  const leftAr = $(this).find('.js-control-slider-product__left').addClass(`js-left${indexInArray}`);
  const leftRh = $(this).find('.js-control-slider-product__right').addClass(`js-right${indexInArray}`);

  const galleryThumbs = new Swiper(smallSlider, {
    spaceBetween: 0,
    slidesPerView: 'auto',
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
      // Disable preloading of all images
    preloadImages: false,
    // Enable lazy loading
    lazy: true,
     // Navigation arrows
     navigation: {
      nextEl: leftRh,
      prevEl: leftAr,
    },
  });
  // eslint-disable-next-line no-unused-vars
  const galleryTop = new Swiper(bigSlider, {
    spaceBetween: 0,
      // Disable preloading of all images
  preloadImages: false,
  // Enable lazy loading
  lazy: true,
    slidesPerView: 1,
    // navigation: {
    //   nextEl: '.swiper-button-next',
    //   prevEl: '.swiper-button-prev',
    // },
    thumbs: {
      swiper: galleryThumbs
    },
    navigation: {
      nextEl: leftRh,
      prevEl: leftAr,
    },
  });

});


});

  
